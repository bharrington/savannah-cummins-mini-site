<?php the_post(); ?>
<?php get_template_part( 'partials/common/_header' ); ?>

<section class="portfolio__single">
  <article>
    <?php if( have_rows('portfolio_item') ): ?>
    <div class="portfolio__slider">
      <?php while( have_rows('portfolio_item') ): the_row();
        $img         = get_sub_field('image');
        $has_video   = get_sub_field('video_show');
        $video       = get_sub_field('video');
        $img_resized = wp_get_attachment_image_src($img, 'very_large'); ?>

        <div class="item<?php if ( $has_video ): ?> __has-video<?php endif; ?>">
          <?php if ( $has_video ): ?>
            <?php echo $video; ?>
          <?php else: ?>
            <?php if ( $img ): ?>
              <img src="<?php echo $img_resized[0]; ?>">
            <?php endif; ?>
        <?php endif; ?>
        </div>
      <?php endwhile; ?>
    </div>
    <?php else: ?>
      <div class="item item__coming-soon">
        <h2>Coming Soon</h2>
      </div>
    <?php endif; ?>
  </article>
</section>

<?php get_template_part( 'partials/common/_footer' ); ?>
