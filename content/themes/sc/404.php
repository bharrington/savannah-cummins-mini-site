<?php get_template_part( 'partials/common/_header' ); ?>

<section class="error-404">
  <h2>Ooops, Error.</h2>
</section>

<?php get_template_part( 'partials/common/_footer' ); ?>
