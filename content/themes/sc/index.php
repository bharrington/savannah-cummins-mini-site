<?php
  $portfolio_args = array(
    'posts_per_page' => -1,
    'post_type'      => 'portfolio'
  );

  $port = new WP_Query( $portfolio_args );
?>

<?php get_template_part( 'partials/common/_header' ); ?>

<section class="portfolio__listing">

  <?php while ($port->have_posts()) : $port->the_post(); ?>
  <?  $img         = get_field('group_thumbnail');
      $img_resized = wp_get_attachment_image_src($img, 'category');
  ?>
  <article class="portfolio__item">
    <a href="<?php the_permalink(); ?>">
      <div class="portfolio__item-overlay"><p><?php the_title(); ?></p></div>
      <img src="<?php echo $img_resized[0]; ?>" />
    </a>
  </article>
  <?php endwhile; wp_reset_query(); ?>

</section>

<?php get_template_part( 'partials/common/_footer' ); ?>
