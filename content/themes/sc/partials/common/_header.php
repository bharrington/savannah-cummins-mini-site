<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8">
  <title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

  <?php wp_head(); ?>

</head>

<?php $custom_class = 'custom-class'; ?>
<body <?php body_class("$custom_class"); ?> >

  <div class="menu__reveal">menu</div>
  <header>
    <h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
    <nav role="navigation" class="menu__primary">
      <?php get_template_part( 'partials/common/_menu-header' ); ?>
    </nav>

    <nav role="navigation" class="menu__social">
      <?php get_template_part( 'partials/common/_menu-social' ); ?>
    </nav>
  </header>
