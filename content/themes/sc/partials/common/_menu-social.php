<?php
  $fb      = get_field('link_facebook', 'option');
  $tw      = get_field('link_twitter', 'option');
  $ig      = get_field('link_instagram', 'option');
  $yt      = get_field('link_youtube', 'option');
  $vim     = get_field('link_vimeo', 'option');
  $li      = get_field('link_linkedin', 'option');
  $email   = get_field('contact_email', 'option');
  $social_menu = get_field('show_social_menu', 'option');
?>

<?php if( $social_menu ): ?>
  <nav role="navigation" class="menu__social">
    <ul>
      <?php if( $fb ): ?>
      <li><a class="facebook symbol" target="_blank" href="<?php echo $fb; ?>">&#xe227;</a></li>
      <?php endif; ?>

      <?php if( $tw ): ?>
      <li><a class="twitter symbol" target="_blank" href="<?php echo $tw; ?>">&#xe286;</a></li>
      <?php endif; ?>

      <?php if( $ig ): ?>
      <li><a class="instagram symbol" target="_blank" href="<?php echo $ig; ?>">&#xe300;</a></li>
      <?php endif; ?>

      <?php if( $yt ): ?>
      <li><a class="youtube symbol" target="_blank" href="<?php echo $yt; ?>">&#xe299;</a></li>
      <?php endif; ?>

      <?php if( $vim ): ?>
      <li><a class="vimeo symbol" target="_blank" href="<?php echo $vim; ?>">&#xe289;</a></li>
      <?php endif; ?>

      <?php if( $li ): ?>
      <li><a class="linkedin symbol" target="_blank" href="<?php echo $li; ?>">&#xe252;</a></li>
      <?php endif; ?>

      <?php if( $email ): ?>
      <li><a class="email symbol" target="_blank" href="mailto:<?php echo $email; ?>">&#xe224;</a></li>
      <?php endif; ?>
    </ul>
  </nav>
<?php endif; ?>
