<div class="video-wrapper"><?php the_sub_field('video'); ?></div>

<?php if( get_sub_field('video_text') ): ?>
  <p class="small-text"><?php the_sub_field('video_text'); ?></p>
<?php endif; ?>
