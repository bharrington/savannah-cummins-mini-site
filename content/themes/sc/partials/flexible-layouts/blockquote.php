<blockquote>
  <?php the_sub_field('quote_text'); ?>

  <?php if( get_sub_field('quoter') ): ?>
  <cite>
    <?php the_sub_field('quoter'); ?>
  </cite>
  <?php endif; ?>
</blockquote>
