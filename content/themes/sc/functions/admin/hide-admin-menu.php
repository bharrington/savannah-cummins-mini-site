<?php
add_action('admin_menu', 'remove_admin_menu_items');

add_action('admin_menu','my_admin_menu');
function my_admin_menu() {
  remove_admin_menu_section('edit-comments.php');
  remove_admin_menu_section('edit.php');
}

function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;

  return array(
    'index.php', // Dashboard
    'separator2', // Second separator
    'edit.php?post_type=portfolio',
    'edit.php?post_type=page',
    'separator1', // First separator
    // 'edit.php', // Posts
    'upload.php', // Media
  );
}

add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');
