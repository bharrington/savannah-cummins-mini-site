<?php

// acf collapse image shrink
add_action('admin_head', 'sc_admin_style');
function sc_admin_style() {
  echo '<style>
    tr.acf-row.-collapsed img {
      width: 200px;
    }
  </style>';
}
