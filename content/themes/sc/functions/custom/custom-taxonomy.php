<?php
// add portfolio tax
add_action( 'init', 'sc_portfolio_tax' );
function sc_portfolio_tax() {
  register_taxonomy(
    'portfolio_category',
    'portfolio',
    array(
      'label' => __( 'Portfolio Category' ),
      'rewrite' => array( 'slug' => 'portfolio' ),
      'hierarchical' => true,
      'show_admin_column' => true,
      'query_var' => true
    )
  );
}

?>
