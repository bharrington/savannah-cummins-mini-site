<?php
add_action( 'after_setup_theme', 'sc_custom_images' );
function sc_custom_images() {
  add_image_size( 'very_large', 1600, 9999 );  // used for image slider
  add_image_size( 'medium', 950, 9999 );       // used for mini slider
  add_image_size( 'category', 800, 9999 );     // used for image slider admin
}
