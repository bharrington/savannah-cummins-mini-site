<?php
function sc_register_menus() {
  register_nav_menus(
    array(
    'menu-header' => __('Header Menu')
    )
  );
}

add_action('init', 'sc_register_menus');
