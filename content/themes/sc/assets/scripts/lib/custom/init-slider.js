$( document ).ready(function() {
  if ($('.mini-slider.owl-carousel').length) {
    $(".mini-slider.owl-carousel").bxSlider({
      items : 3,
      lazyLoad : true,
      navigation : false,
      slideSpeed : 600,
      paginationSpeed : 400,
      autoHeight : true
    });
  }

  if ($('.full-slider.owl-carousel').length) {
    $(".full-slider.owl-carousel").bxSlider({
      navigation : false,
      pagination: false,
      lazyLoad : true,
      slideSpeed : 600,
      paginationSpeed : 400,
      singleItem : true,
      autoHeight : true
    });
  }

  if ($('.portfolio__single .portfolio__slider').length) {
    var $slider   = $('.portfolio__single .portfolio__slider'),

    sliderOptions = {
        pager: false,
        keyboardEnabled: true,
        video: true
    };

    enquire.register("screen and (min-width: 1200px)", {
      match : function() {
        $slider.bxSlider(sliderOptions);
      },
      unmatch : function() {
        $slider.destroySlider();
      }
    });

    enquire.register("screen and (max-width: 1199.9px)", {
      match : function() {
        $slider.addClass('__mq-sm');
      },
      unmatch : function() {
        $slider.removeClass('__mq-sm');
        $slider.bxSlider(sliderOptions);
      }
    });
  }
});
