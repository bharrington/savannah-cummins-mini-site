<?php the_post(); ?>
<?php get_template_part( 'partials/common/_header' ); ?>

<section class="text-page has-sidebar">
  <article class="content">
    <?php if( get_field('flexible_content') ):
      while ( has_sub_field('flexible_content') ) :
        $rowName = get_row_layout();
        echo '<div class="row '.$rowName.'">';
        include(dirname(__FILE__) . '/partials/flexible-layouts/'.$rowName.'.php');
        echo '</div>';
      endwhile;
    endif;?>
  </article>
</section>

<?php get_template_part( 'partials/common/_footer' ); ?>
