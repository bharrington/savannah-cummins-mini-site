## Install Node Via Homebrew
```sh
$ brew install node
```

## Install Node Commandline Interface
```sh
$ npm install cli
```

## Download & Install Production version of Bless
https://github.com/paulyoung/bless.js/tree/4.0.0-development
Change to the directory of the download.

```sh
npm install bless.js-4.0.0-development -g
```

## Install Grunt Globally
```sh
$ npm install -g grunt-cli
```

## Install Modules
Change to the directory of the project.
```sh
$ npm install
```

### Gruntfile commands
```sh
$ grunt watch
$ grunt crush
```

- watch : watches files and runs concat and compass jobs
* grunt watch has livereload built in.
- crush : runs imagemin to compress images
